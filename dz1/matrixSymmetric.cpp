#include <iostream>
#include "lib.h"
using namespace std;
void matrixSymmetric(int** matrixA, short matrixSize)
{
    short enumerator = 0;
    for (int i = 0; i < (matrixSize % 2); i++)
    {
        for (int j = 0; j < (matrixSize % 2); j++)
        {
            if ((i == j) && (matrixA[i][j] == matrixA[matrixSize - i][matrixSize - j]))
            {
                enumerator++;
            }
        }
    }
    if (enumerator != 5)
    {
        cout << "main diagonal is not symmetric";
    }
    else
    {
        cout << "main diagonal is symmetric";
    }
}