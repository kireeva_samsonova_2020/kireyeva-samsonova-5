#include <iostream>
#include "lib.h"
using namespace std;
void matrixSum(int** matrixA, short matrixSize)
{
    int sum = 0;
    for (int i = (matrixSize / 5); i < (matrixSize / 5) * 4 - 1; i++)
    {
        for (int j = (matrixSize / 5); j < (matrixSize / 5) * 4 - 1; j++)
        {
            if (matrixA[i][j] > 0)
            {
                sum += matrixA[i][j];
            }
        }
    }
    cout << "sum of the elements = " << sum;
}