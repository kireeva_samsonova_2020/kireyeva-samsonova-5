#include <iostream>
#include "lib.h"
using namespace std;
void outMatrix(int** matrixA, short matrixSize)
{
    for (int i = 0; i < matrixSize; i++)
    {
        for (int j = 0; j < matrixSize; j++)
        {
            cout << matrixA[i][j] << "\t";
        }
        cout << endl;
    }
}