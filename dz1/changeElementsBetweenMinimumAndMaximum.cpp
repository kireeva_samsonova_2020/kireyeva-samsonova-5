#include <iostream>
#include "lib.h"
void changeElementsBetweenMinimumAndMaximum(int** matrixA, short matrixSize)
{
    int max, numberMax = 0, min, numberMin = 0, b = 0, g = 0, c = 0;
    for (int i = 0; i < matrixSize; i++)
    {
        max = matrixA[i][0];
        for (int j = 1; j < matrixSize; j++)
        {
            if (matrixA[i][j] > max)
            {
                max = matrixA[i][j];
                numberMax = j;
            }
        }
        min = matrixA[i][0];
        for (int j = 1; j < matrixSize; j++)
        {
            if (matrixA[i][j] < min)
            {
                min = matrixA[i][j];
                numberMin = j;
            }
        }
        if ((numberMin < numberMax) && (abs(numberMin - numberMax) >= 2))
        {
            g = numberMax;
            for (int j = numberMin + 1; j <= numberMax - 1; j++)
            {
                if (b < g)
                {
                    b = numberMin + 1;
                    g--;
                    c = matrixA[i][b];
                    matrixA[i][b] = matrixA[i][g];
                    matrixA[i][g] = c;
                }
            }
        }
        else
        {
            if (abs(numberMin - numberMax) >= 2)
            {
                b = numberMin;
                for (int j = numberMax + 1; j <= numberMin - 1; j++)
                {
                    if (g < b)
                    {
                        b--;
                        g = numberMax + 1;
                        c = matrixA[i][b];
                        matrixA[i][b] = matrixA[i][g];
                        matrixA[i][g] = c;
                    }
                }
            }
        }
    }
    outputMatrix(matrixA, matrixSize);
}