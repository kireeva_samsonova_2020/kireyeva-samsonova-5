#include <iostream>
#include <random>
#include "lib.h"
using namespace std;
int** createMatrix(short matrixSize)
{
    int** matrixA = new int* [matrixSize];
    for (int i = 0; i < matrixSize; i++)
    {
        matrixA[i] = new int[matrixSize];
    }
    for (int i = 0; i < matrixSize; i++)
    {
        for (int j = 0; j < matrixSize; j++)
        {
            matrixA[i][j] = rand() % (MAX_RANDOM_VALUE - MIN_RANDOM_VALUE + 1) + MIN_RANDOM_VALUE;
        }
    }
    cout << "matrix has been successfully created";
    return matrixA;
}