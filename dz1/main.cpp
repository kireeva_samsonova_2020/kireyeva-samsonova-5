﻿#include "lib.h"
using namespace std;
int main()
{
    srand(time(0));
    cout << "Enter matrix size: ";
    unsigned short matrixSize;
    cin >> matrixSize;
    system("cls");
    int** matrixA = 0;
    unsigned short punkt;
    vector<string> menu = { "Create matrix", "Output matrix", "Change elements between minimum and maximum", "Matrix symmetric", "Matrix sum" , "Exit" };
    do
    {
        menuToScreen(menu);
        cin >> punkt;
        system("cls");
        switch (punkt) {
        case 1:
        {
            matrixA = createMatrix(matrixSize);
            break;
        }
        case 2:
        {
            outputMatrix(matrixA, matrixSize);
            break;
        }
        case 3:
        {
            changeElementsBetweenMinimumAndMaximum(matrixA, matrixSize);
            break;
        }
        case 4:
        {
            matrixSymmetric(matrixA, matrixSize);
            break;
        }
        case 5:
        {
            matrixSum(matrixA, matrixSize);
            break;
        }
        case 6:
        {
            exit(0);
            break;
        }
        }
        cout << endl << "Press any key to return to menu";
        char exitSymbol = _getch();
        system("cls");
    } while (punkt != menu.size());
    return 0;
}